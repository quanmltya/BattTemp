package uk.org.bitservices.batttemp;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;

public class BattTempService extends Service {

	// Application Context
	private Context conApp = null;
	
	// Notification Icon
	private NotificationManager nMan = null;
    private NotificationCompat.Builder nBuild = null;
	private PendingIntent nPendingIntent = null;
	private static final int NOTIFICATION_ID = 1;	

	// Current Temperature (Raw)
	private float fTempC = 999;
	private int iLastIcon = 999;
	
	@Override
	public IBinder onBind(Intent intent) {
		// TODO Auto-generated method stub
		return null;
	}

@Override
public void onCreate() {
	// Initialise Context (to this FIRST)
	conApp = this.getApplicationContext();
	
	// Reset Variables
	iLastIcon = 999;
	
    // Initialise Notification
    nMan = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
    nPendingIntent = PendingIntent.getActivity(conApp, 0, new Intent(conApp, ShowTemp.class), 0);
    
    nBuild = new NotificationCompat.Builder(this);
    nBuild.setContentTitle("Battery Temperature");
    nBuild.setContentIntent(nPendingIntent);
    nBuild.setOnlyAlertOnce(true);
    nBuild.setOngoing(true);
}

@Override
public void onDestroy() {
	// Kill Notification
	try
	{
		nMan.cancel(NOTIFICATION_ID);
	} finally {
		nBuild = null;
		nPendingIntent = null;
		nMan = null;
		conApp = null;
	}
}

@Override
public int onStartCommand(Intent intent, int flags, int startId) {
	// Get Battery Temperature
	int iIcon = GetTempUpdate();
	
	if ((iIcon == iLastIcon) && (iLastIcon != 999)) {
		// Battery Temperature has not changed! Do not update.
	} else {
		String strCurTemp = "";
		String strCurStatus = "";
		
		if (fTempC >= 45) {
			strCurStatus = "(Overheat)";
		} else if (fTempC <= 0) {
			strCurStatus = "(Freezing)";
		} else {
			if (fTempC >= 40) {
				strCurStatus = "(Hot)";
			} else if (fTempC <= 5) {
				strCurStatus = "(Cold)";
			} else {
				if (fTempC >= 30) {
					strCurStatus = "(Warm)";
				} else if (fTempC <=15) {
					strCurStatus = "(Cool)";
				} else {
					strCurStatus = "(Optimal)";
				}
			}
		}
		
		if (fTempC == 999) {
			strCurTemp = "Unknown°C";
			strCurStatus = "(Error)";
		} else {
			strCurTemp = String.valueOf(fTempC) + "°C";
		}
	
		// Start Notification
		SetNotificationParams(iIcon, strCurStatus + " " + strCurTemp);
		nMan.notify(NOTIFICATION_ID, nBuild.build());
	}
	
	iLastIcon = iIcon;
	
	// Stop service when done.
    return START_NOT_STICKY;
}

private void SetNotificationParams(int iIcon, String value) {
	nBuild.setWhen(System.currentTimeMillis());
	nBuild.setSmallIcon(iIcon);
	nBuild.setContentText(value);
}

private int GetTempUpdate() {
	int iResult = R.drawable.e;
	int iTempC = 999;
	fTempC = BatteryStats.getBatteryTempC(conApp);
	iTempC = Math.round(fTempC);
	
	if (iTempC == 999)
	{
		iResult = R.drawable.e;
	} else {
		if (iTempC < -19) {
			iResult = R.drawable.m20;
		} else if (iTempC > 99) {
			iResult = R.drawable.p100;
		} else {
			if (iTempC < 0) {
				iResult = getResources().getIdentifier("m" + String.valueOf(Math.abs(iTempC)), "drawable", "uk.org.bitservices.batttemp");
			} else {
				iResult = getResources().getIdentifier("p" + String.valueOf(iTempC), "drawable", "uk.org.bitservices.batttemp");
			}
		}
	}
	
	return iResult;
}

}

